#!/bin/bash
# Append each line of each file in a file result
#

function usage() {
	echo "usage $0: output file1..file12"
}

function merge () {
  local output=$1
  shift
  local file1=$1
  local file2=$2
  local file3=$3
  local file4=$4
  local file5=$5
  local file6=$6  

  while read lineF1 <&3 && read lineF2 <&4 && read lineF3 <&5 && read lineF4 <&6 && read lineF5 <&7 && read lineF6 <&8
  do
        echo "${lineF1}${lineF2}${lineF3}${lineF4}${lineF5}${lineF6}"
  done 3<$file1 4<$file2 5<$file3 6<$file4 7<$file5 8<$file6 > $output

}

if  [ $# -ne 13 ]; then
	usage
	exit 1
fi

output=$1
shift
merge $output.1 $1 $2 $3 $4 $5 $6 
shift 6   
merge $output.2 $1 $2 $3 $4 $5 $6 
# reste a concatener $output.1 et $output.2

#while [[ $# > 0 ]]; do 
#  merge $output.1 $1 $2 $3 $4 $5 $6 
#  shift 6   
#done